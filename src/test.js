

/*!
 * recast.js
 * https://github.com/vincent/recast.js
 *
 * Copyright 2014 Vincent Lark
 * Released under the MIT license
 */
/*jshint onevar: false, indent:4 */
/*global exports: true, require: true, Buffer: true */
'use strict';

var fs       = require('fs');
const recast = require("recastjs");
var settings = require('./settings');


    settings(recast);

    recast.OBJLoader('./models/interior/scene.obj', function(){

        recast.buildTiled();

        recast.saveTileMesh('./navmesh.bin', recast.cb(function (error, serialized) {

            if (fs.writeFile) {

                var buffer = new Buffer(serialized.length);

                for (var i = 0; i < serialized.length; i++) {
                    buffer.writeUInt8(serialized[i], i);
                }

                fs.writeFile('./navmesh.bin', buffer, function (err) {
                    if (err) 
                        throw err;
                    console.log('done');
                });

            } else {
                console.log('not write file');
            }
            
        }));
    });
