//var THREE = window.THREE = require('three');
//require('three/examples/js/loaders/GLTFLoader');
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 10000 );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );


// add controller
orbitControls = new THREE.OrbitControls(camera, renderer.domElement);
orbitControls.target = new THREE.Vector3(0, 0, 0);//控制焦点
orbitControls.autoRotate = false;//将自动旋转关闭
clock = new THREE.Clock();

// add pathfinding
THREE.Pathfinding = threePathfinding.Pathfinding;
THREE.PathfindingHelper = threePathfinding.PathfindingHelper;
const helper = new THREE.PathfindingHelper();
scene.add(helper)
const playerPosition = new THREE.Vector3( 0, 0, 0 );
const targetPosition = new THREE.Vector3();

let path;

helper.setPlayerPosition( playerPosition )
      .setTargetPosition( playerPosition );

//navmesh = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({
//    color: Color.NAVMESH,
//    opacity: 0.75,
//    transparent: true
//}));
//
//scene.add(navmesh);

// add axes helper
var axesHelper = new THREE.AxesHelper( 5000 ); 
scene.add( axesHelper );

var light = new THREE.AmbientLight( 0xffffff ); // soft white light
scene.add( light );

var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
scene.add( directionalLight )

const loader = new THREE.GLTFLoader();
var obj = null;
/*
loader.load(
    'scene.gltf',
    ( gltf ) => {
        // called when the resource is loaded
        obj = gltf.scene;
        scene.add( obj );
        console.log("add to the scene")
    },
    ( xhr ) => {
        // called while loading is progressing
        console.log( `${( xhr.loaded / xhr.total * 100 )}% loaded` );
    },
    ( error ) => {
        // called when loading has errors
        console.error( 'An error happened', error );
    },
);
*/
/*
var geometry = new THREE.BoxGeometry( 1, 1, 1 );
var material = new THREE.MeshPhongMaterial( { color: 0x00ff00 } );
var cube = new THREE.Mesh( geometry, material );
scene.add( cube );

var animate = function () {
    requestAnimationFrame( animate );

    obj.rotation.x += 0.01;
    obj.rotation.y += 0.01;

    renderer.render( scene, camera );
};

animate();
*/
camera.position.x = 0;
camera.position.y = 200;
camera.position.z = 4000;

var render = function () {
    
    delta = clock.getDelta();
    orbitControls.update(delta);
    tick(delta);

    requestAnimationFrame( render );
    renderer.render(scene,camera);
};

function tick ( dt ) {
    if ( !obj || !(path||[]).length ) return

    let targetPosition = path[ 0 ];
    const velocity = targetPosition.clone().sub( playerPosition );

    if (velocity.lengthSq() > 0.05 * 0.05) {
        velocity.normalize();
        // Move player to target
        playerPosition.add( velocity.multiplyScalar( dt * SPEED ) );
        helper.setPlayerPosition( playerPosition );
    } else {
        // Remove node from the path we calculated
        path.shift();
    }
}


function onDocumentMouseUp (event) {

    const mouse = new THREE.Vector2();
    const raycaster = new THREE.Raycaster();

    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

    camera.updateMatrixWorld();

    raycaster.setFromCamera( mouse, camera );

    const intersects = raycaster.intersectObject( navmesh );

    if ( !intersects.length ) return;

    targetPosition.copy( intersects[0].point );

    helper
        .reset()
        .setPlayerPosition( playerPosition );

    // Teleport on ctrl/cmd click or RMB.
    if (event.metaKey || event.ctrlKey || event.button === 2) {

        path = null;
        groupID = pathfinder.getGroup(ZONE, targetPosition, true);
        const closestNode = pathfinder.getClosestNode( playerPosition, ZONE, groupID, true );

        helper.setPlayerPosition( playerPosition.copy( targetPosition ) )
        if ( closestNode ) helper.setNodePosition( closestNode.centroid );

        return;

    }

    const targetGroupID = pathfinder.getGroup( ZONE, targetPosition, true );
    const closestTargetNode = pathfinder.getClosestNode( targetPosition, ZONE, targetGroupID, true );

    helper.setTargetPosition( targetPosition );
    if (closestTargetNode) helper.setNodePosition( closestTargetNode.centroid );

    // Calculate a path to the target and store it
    path = pathfinder.findPath( playerPosition, targetPosition, ZONE, groupID );

    if ( path && path.length ) {

        helper.setPath( path );

    } else {

        const closestPlayerNode = pathfinder.getClosestNode( playerPosition, ZONE, groupID );
        const clamped = new THREE.Vector3();

        // TODO(donmccurdy): Don't clone targetPosition, fix the bug.
        pathfinder.clampStep(
            playerPosition, targetPosition.clone(), closestPlayerNode, ZONE, groupID, clamped );

        helper.setStepPosition( clamped );

    }
}

document.addEventListener( 'mouseup', onDocumentMouseUp, false );

render();

